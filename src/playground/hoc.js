// Higher Order Component (HOC) - a component that renders another component
// Reuse code
// Render hijacking
// Prop manipulation
// Abstract state

import React from 'react';
import ReactDOM from 'react-dom';

const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p>The info is {props.info}</p>
    </div>
);

const withAdminWarning = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAdmin &&  
            <p>This is private info </p> }
            <WrappedComponent {...props}/>
            
        </div>
    );
};

const requireAuthentification = (WrappedComponent) => {
    return (props) => (
        <div>
        {props.isAuthenticated ? (
            <div>
            <p>Auth OK</p>
            <WrappedComponent {...props} />
            </div>
        ) : (
            <p>Please log in</p>
        )}
        </div>
    );
}
const AdminInfo = withAdminWarning(Info);
const AuthInfo = requireAuthentification(Info);

// ReactDOM.render(<AdminInfo isAdmin={true} info="these are the details"/>, document.getElementById('app'));
ReactDOM.render(<AuthInfo isAuthenticated={false} info="these are the details"/>, document.getElementById('app'));