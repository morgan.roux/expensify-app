

//
// Object destructuring
//
const person = {
    name: 'Morgan',
    age: 30,
    location: {
        city: 'Paris',
        temp: 3
    }
}


const {name: firstName = 'Anonymous', age} = person;
// const name = person.name;
// const age = person.age;

console.log(`${firstName} has ${age}`);

const {cityOfBirth, temp: temperature} = person.location;
if(temperature && city)
{
    console.log(`It's ${temperature} in ${cityOfBirth}`);
}

const book = {
    title: 'Ego in the Enemy',
    author: 'Ryan Holiday',
    publisher: {
        name: 'Penguin',

    }
};

const {name: publisherName = 'self-published'} = book.publisher;
console.log(publisherName);


//
// Array destructuring
//


const address = ['27 rue Viala', 'Paris', 'France', '75015'];

//const [street, city, state, zip] = address;
const [ ,city, ,zip = '0000'] = address;

console.log(`your city : ${city} and zip: ${zip}`);


const item = ['coffee', '$2.00', '$2.50', '$2.75'];
const [itemName, , mediumPrice] = item;
console.log(`A medium ${itemName} cost ${mediumPrice}`);