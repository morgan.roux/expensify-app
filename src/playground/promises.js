
const promise = new Promise((resolve, reject)=> {
    setTimeout( () => {
         resolve('this is my resolve data');
         //reject('error mssg')
    },1500);
    
});
console.log('before');

promise.then((data) => {
    console.log(data);

    return new Promise((resolve, reject)=> {
        setTimeout( () => {
              resolve('this is my other promise');
        },1500);
    });
}).then((str) => {
    console.log(str);
    return 'and a new string';
}).then((str) => {
    console.log(str);

}).catch((data) => {
    console.log('error : ', data);
});

console.log('after');