import * as firebase from 'firebase';
import expenses from '../tests/fixtures/expenses'

const config = {
    apiKey: "AIzaSyCJ665aEMuTjUl4tadpkZPkJhq0FqJ52p0",
    authDomain: "expensify-f39d4.firebaseapp.com",
    databaseURL: "https://expensify-f39d4.firebaseio.com",
    projectId: "expensify-f39d4",
    storageBucket: "expensify-f39d4.appspot.com",
    messagingSenderId: "69303772935"
};

firebase.initializeApp(config);

const database = firebase.database();

// Create array with ids
// database.ref('expenses').push(expenses[0]);
// database.ref('expenses').push(expenses[1]); 
// database.ref('expenses').push(expenses[2]);

// child_removed
database.ref('expenses').on('child_removed', (snapshot) => {
    console.log(snapshot.key, snapshot.val());
});

// child_changed
database.ref('expenses').on('child_changed', (snapshot) => {
    console.log(snapshot.key, snapshot.val());
});

// child_added
database.ref('expenses').on('child_added', (snapshot) => {
    console.log(snapshot.key, snapshot.val());
});


// Subscribe and fetch array
// database.ref('expenses')
//     .on('value', (snapshot) => {
//         const expenses = [];
//         snapshot.forEach((childSnapshot) => {
//             expenses.push({
//                 id: childSnapshot.key,
//                 ...childSnapshot.val()
//             });
//         });
//         console.log(expenses);
//     });

// exemple working with array
// database.ref('notes/-LV3NmGAYa5c3TYoGRxQ').remove();

// database.ref('notes').push({
//     title: 'Course Topics',
//     body: 'newww'
// });

// Subscription
// const onValueChange = database.ref().on('value',(snapshot) => {
//     const val = snapshot.val();
//     console.log(`${val.name} is a ${val.job.title} at ${val.job.company}`);
// }, (e) => {
//     console.log('error', e);
// });

// Fetching data once
// database.ref('location/city')
//     .once('value')
//     .then((snapshot) => {
//         const val = snapshot.val();
//         console.log(val);
//     }).catch((e) => {
//         console.log('error : ', e);
//     })

// Setting a database
// database.ref().set({
//     name: 'Morgan Roux',
//     age: 26,
//     stressLevel: 6,
//     job: {
//         title: 'Software developer',
//         company: 'Google'
//     },
//     isSingle: false,
//     location: {
//         city: 'Paris',
//         country: 'France'
//     }
//   }).then(() => {
//       console.log('data saved');
//   }).catch((e) => {
//       console.log('error : ', e);
//   });

//   database.ref().update({
//     stressLevel: 9,
//     'job/company': 'Amazon',
//     'location/city' : 'Seattle'
//    });
// database.ref()
//     .remove()
//     .then(() => {
//         console.log('remove ok');
//     }).catch((e) => {
//         console.log('remove Nok : ', e)
//     });

//   database.ref('isSingle')
//     .set(null)
//     .then(() => {
//         console.log('null ok');
//     }).catch((e) => {
//         console.log('null Nok : ', e)
//     });