

export const getExpensesTotal = (expenses = []) => {

    return expenses.reduce((total, {amount}) => {
        return total += amount;
    },0);

};

export const getExpensesCount = (expenses = []) => {

    return expenses.length;

};