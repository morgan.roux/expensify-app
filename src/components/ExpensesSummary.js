import React from 'react';
import { connect } from 'react-redux';
import selectExpenses from '../selectors/expenses';
import { getExpensesTotal, getExpensesCount} from '../selectors/expenses-total';
import numeral from 'numeral';


export const ExpensesSummary  = ({count, total}) => (
    <div>
        {
            count === 0 || (
                <p>
                    {`Viewing ${count} ${count === 1 ? 'expense' : 'expenses'} totalling ${numeral(total).format('$0,0.00')}`}
                </p>
            )
        }
    </div>
);



const mapStateToProps = (state) => {
    const expenses= selectExpenses(state.expenses, state.filters);
    return {
        count: getExpensesCount(expenses),
        total: getExpensesTotal(expenses) / 100
    }
};

export default connect(mapStateToProps)(ExpensesSummary)