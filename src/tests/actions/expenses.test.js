import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { 
    addExpense, 
    removeExpense, 
    editExpense, 
    startAddExpense, 
    setExpenses, 
    startSetExpenses, 
    startRemoveExpense, 
    startEditExpense
} from '../../actions/expenses';
import expenses from '../fixtures/expenses';
import database from '../../firebase/firebase';

const uid = 'testuid';
const defaultAuthState = { auth:{ uid } }
const createMockStore = configureMockStore([thunk]);

beforeEach((done) => {
    const expensesData = {};
    expenses.forEach(({ id, description, notes, amount, createdAt }) => {
        expensesData[id] = { description, notes, amount, createdAt };
    });
    database.ref(`users/${uid}/expenses`).set(expensesData).then(()=>done());
});


test('should setup removeExpense action object', () => {
    const action = removeExpense({ id : '123abc' });

    expect(action).toEqual({
        type: 'REMOVE_EXPENSE',
        id: '123abc'
    });
});

test('should remove expense from database', (done) => {
    const store = createMockStore(defaultAuthState);
    const id = expenses[0].id;
    store.dispatch(startRemoveExpense({ id }))
    .then(()=>{
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: 'REMOVE_EXPENSE',
            id
        });
        return database.ref(`users/${uid}/expenses/${expenses[0].id}`).once('value')
        .then((snapshot)=>{
            expect(snapshot.val()).toBeNull();
            done();
        });
    });
});

test('should setup editExpense action object', () => {
    const action = editExpense( '123abc', {notes: 'new  note'});

    expect(action).toEqual({
        type: 'EDIT_EXPENSE',
        id: '123abc',
        update: {notes: 'new  note'}
    });
});

test('should update database', (done) => {
    const store = createMockStore(defaultAuthState);
    const {description, id, createdAt, notes, amount } = expenses[1];
    const update = { notes: 'new notes !!', amount:100 }
    store.dispatch(startEditExpense(id, update ))
    .then( () => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: 'EDIT_EXPENSE',
            id,
            update
        });

        return database.ref(`users/${uid}/expenses/${id}`).once('value') 
        .then((snapshot) => {
            expect(snapshot.val()).toEqual({
                description,
                amount,
                notes,
                createdAt,
                ...update
            });
            done();
        })
    })
});

test('should setup addExpense action object with provided values', () => {

    const expenseData = expenses[2];
    const action = addExpense(expenseData);
    expect(action).toEqual({
        type: 'ADD_EXPENSE',
        expense : expenses[2]
    });
});

test('should add expense to database and store', (done) => {
    const store = createMockStore(defaultAuthState);
    const expenseData = {
        description: 'Mouse',
        amount: 3000,
        notes : 'notes here',
        createdAt: 1000
    };

    store.dispatch(startAddExpense(expenseData)).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type : 'ADD_EXPENSE',
            expense: {
                id: expect.any(String),
                ...expenseData
            }
        });

        return database.ref(`users/${uid}/expenses/${actions[0].expense.id}`).once('value');
    }).then((snapshot) => {
        expect(snapshot.val()).toEqual(expenseData);
        done();
    });
});

test('should add expense with defaults to database and store', (done) => {
    const store = createMockStore(defaultAuthState);
    const expenseData = {
        description: '', 
        notes: '', 
        amount: 0, 
        createdAt: 0
    };
    store.dispatch(startAddExpense()).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: 'ADD_EXPENSE',
            expense: {
                id: expect.any(String),
                ...expenseData
            }
        });

        return database.ref(`users/${uid}/expenses/${actions[0].expense.id}`).once('value');
    }).then((snapshot) => {
        expect(snapshot.val()).toEqual(expenseData);
        done();
    });

});

test('should setup set expenses action object with data',() => {
    const action = setExpenses(expenses);
    expect(action).toEqual({
        type: 'SET_EXPENSES',
        expenses
    }); 
});

test('should fetch data from firbase and set the store with expenses', (done) => {
    const store = createMockStore(defaultAuthState);
    store.dispatch(startSetExpenses()).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: 'SET_EXPENSES',
            expenses
        });
        done();
    });
});