import moment from 'moment'
import {setStartDate, setEndDate, setTextFilter, sortByDate, sortByAmount} from '../../actions/filters';

test('should generate set start date action obejct', () => {
    const action = setStartDate(moment(0));

    expect(action).toEqual({
        type: 'SET_START_DATE',
        date: moment(0)
    });
});

test('should generate set end date action object', () => {
    const action = setEndDate(moment(0));

    expect(action).toEqual({
        type: 'SET_END_DATE',
        date: moment(0)
    })

});

test('sould generate set text filter action object with provided content', () => {
    const text = 'essai';
    const action  = setTextFilter(text);

    expect(action).toEqual({
        type: 'SET_TEXT_FILTER',
        text
    });
});

test('sould generate set text filter action object with default content', () => {
    const action  = setTextFilter();
    

    expect(action).toEqual({
        type: 'SET_TEXT_FILTER',
        text: ''
    });

});

test('sould generate sort by date action object', () => {

    expect(sortByDate()).toEqual({
        type: 'SORT_BY_DATE'
    });
});

test('sould generate sort by amount action object', () => {
    
    expect(sortByAmount()).toEqual({
        type: 'SORT_BY_AMOUNT'
    });

});