import { getExpensesTotal, getExpensesCount } from '../../selectors/expenses-total';
import expenses from '../fixtures/expenses'

test('should return 0 if no expenses', () => {
    const total = getExpensesTotal();
    expect(total).toBe(0);

});

test('sould correctly add up a single expense', () => {
    const total = getExpensesTotal([expenses[0]]);
    expect(total).toBe(95);
});

test('should correctly add up a single expense', () => {
    const total = getExpensesTotal(expenses);
    const trueTotal = 95 + 109500 + 4500;
    expect(total).toBe(trueTotal);

});

test('sould count correctly the number of no expense', () => {
    const count = getExpensesCount();
    expect(count).toBe(0);
});

test ('should correctly count the nnumber of expenses', () => {
    const count = getExpensesCount(expenses);
    expect(count).toBe(3);
});
