import authReducer from '../../reducers/auth'


test('should setup default state',() => {

    const state = authReducer(null,{type: '@@INIT'});
    expect(state).toBeNull();

});

test('should handle LOGIN', () => {
    const uid = 1234;
    const action = {
        type: 'LOGIN',
        uid
    };
    const state = authReducer(null,action);
    expect(state.uid).toBe(uid);

});

test('should handle logout', () => {
    const action = {
        type: 'LOGOUT'
    }

    const state = authReducer({uid: '1234'}, action);
    expect(state).toEqual({});

})

