import expensesReducer from '../../reducers/expenses.js';
import expenses from '../fixtures/expenses';


test('should set default state', () => {

        const state = expensesReducer(undefined, {type: '@@INIT'});
        expect(state).toEqual([]);
});

test('should remove expense by id', () => {

    const action = {type : 'REMOVE_EXPENSE', id: expenses[2].id};
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([expenses[0], expenses[1]]);
});

test('should not remove expense if id not found', () => {

    const action = {type : 'REMOVE_EXPENSE', id: -1};
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([expenses[0], expenses[1], expenses[2]]);
});

test('should edit expense by id', () => {

    const update = {description: 'test passed !'}
    const action = {type : 'EDIT_EXPENSE', id: expenses[2].id, update};
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([expenses[0], expenses[1], {...expenses[2], ...update}]);
});

test('should not edit expense if id not found', () => {

    const update = {description: 'test passed !'};

    const action = {type : 'EDIT_EXPENSE', id: -1, update};
    const state = expensesReducer(expenses, action);
    expect(state).toEqual(expenses);
});

test('should add expense', () => {
    const expense = {
        id: 4,
        description: 'test',
        notes: '',
        amount: 1086,
        createdAt: 100
    }

    const action = {type: 'ADD_EXPENSE', expense};
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([...expenses, expense]);
});

test('should set expenses', () => {
    const action = { type: 'SET_EXPENSES', expenses: expenses[1] };
    const state = expensesReducer(null, action);
    expect(state).toEqual(expenses[1]);
});