import React from 'react'
import { ExpensesSummary } from '../../components/ExpensesSummary';
import { shallow } from 'enzyme';


test('should render ExpensesSummary correctly with no expense', () => {
    const wrapper = shallow(<ExpensesSummary count={0} total={0}/>);
    expect(wrapper).toMatchSnapshot();
});

test('should render ExpensesSummary correctly with 1 expense', () => {
    const wrapper = shallow(<ExpensesSummary count={1} total={109} />);
    expect(wrapper).toMatchSnapshot();
});

test('should render ExpensesSummary correctly with 2 expenses', () => {
    const wrapper = shallow(<ExpensesSummary count={6} total={378901765.97} />);
    expect(wrapper).toMatchSnapshot();

});