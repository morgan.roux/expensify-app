import React from 'react';
import { shallow } from 'enzyme';
import { EditExpensePage } from '../../components/EditExpensePage';
import expenses from '../fixtures/expenses';

let startEditExpense, startRemoveExpense, history, wrapper;

beforeEach ( () => {

    startEditExpense = jest.fn();
    startRemoveExpense = jest.fn();
    history = { push: jest.fn() };
    
    wrapper = shallow(
        <EditExpensePage 
            expense = {expenses[0]} 
            startEditExpense={startEditExpense}
            startRemoveExpense={startRemoveExpense} 
            history= {history}
        />
    );

});

test('should render editExpensePage correctly ', () => {
    expect(wrapper).toMatchSnapshot();
});

test('should handle editExpense', () => {
    
    wrapper.find('ExpenseForm').prop('onSubmit')(expenses[0]);

    expect(startEditExpense).toHaveBeenCalledWith(expenses[0].id, expenses[0]);
    expect(history.push).toHaveBeenCalledWith('/');
});

test('should handle startRemoveExpense', () => {
    wrapper.find('button').simulate('click');
    expect(history.push).toHaveBeenCalledWith('/');
    expect(startRemoveExpense).toHaveBeenCalledWith({id: expenses[0].id});
    

});