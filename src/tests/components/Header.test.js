import React from 'react'
import { Header } from '../../components/Header';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';

let wrapper, startLogout
beforeEach(() => {

    startLogout = jest.fn();
    wrapper = shallow(<Header startLogout={startLogout} />);
})
test ('shoud render Header correctly', () => {
  
    expect(wrapper).toMatchSnapshot();
});

test('should call startLogout onclick', () => {
    wrapper.find('button').simulate('click');
    expect(startLogout).toHaveBeenCalled();
})
