import React from 'react'
import { LoginPage } from '../../components/LoginPage';
import { shallow  } from 'enzyme'

let wrapper, history, startLogin;

beforeEach( () => {
    startLogin = jest.fn();
    history = { push: jest.fn() };
    wrapper = shallow(<LoginPage history={history} startLogin={startLogin}/>);
});

test('should render LoginPage', () => {
    expect(wrapper).toMatchSnapshot();
});

test('should call startLogin onClick', () => {
    wrapper.find('button').simulate('click');
    expect(startLogin).toHaveBeenCalled();
})